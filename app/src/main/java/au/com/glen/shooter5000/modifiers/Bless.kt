package au.com.glen.shooter5000.modifiers

import au.com.glen.shooter5000.database.model.Attack

class Bless : Modifier() {
    init {
        name = "Bless"
        description = "+1 to attacks and saves against fear effects."
    }

    override fun modifyPreAttacks(attacks: MutableList<Attack>): List<Attack> {
        for (attack in attacks) {
            attack.modifier = attack.modifier + 1
        }
        return attacks
    }
}
