package au.com.glen.shooter5000.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context

import au.com.glen.shooter5000.database.converters.IntListConverter
import au.com.glen.shooter5000.database.dao.CharacterDao
import au.com.glen.shooter5000.database.dao.DamageDao
import au.com.glen.shooter5000.database.model.Character
import au.com.glen.shooter5000.database.model.Damage

@Database(entities = arrayOf(Character::class, Damage::class), version = 3)
@TypeConverters(IntListConverter::class)
abstract class CharacterRoomDatabase : RoomDatabase() {

    abstract fun characterDao(): CharacterDao

    abstract fun damageDao(): DamageDao

    companion object {

        private var INSTANCE: CharacterRoomDatabase? = null

        fun getDatabase(context: Context): CharacterRoomDatabase {
            if (INSTANCE == null) {
                synchronized(CharacterRoomDatabase::class.java) {
                    if (INSTANCE == null) {
                        //Create database here
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                CharacterRoomDatabase::class.java, "character_database")
                                .build()
                    }
                }
            }
            return this.INSTANCE!!
        }
    }

}
