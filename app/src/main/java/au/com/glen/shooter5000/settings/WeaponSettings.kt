package au.com.glen.shooter5000.settings

import android.app.Application
import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.ViewGroup
import au.com.glen.shooter5000.R
import au.com.glen.shooter5000.database.model.Character
import au.com.glen.shooter5000.database.model.Damage
import au.com.glen.shooter5000.database.repository.DamageRepository
import java.util.*

class WeaponSettings : AppCompatActivity() {

    internal lateinit var adapter: WeaponAdapter

    internal lateinit var repo: DamageRepository

    internal lateinit var character: Character

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            0 -> {
                val launchProductList = Intent(this, WeaponSettings::class.java)
                startActivity(launchProductList)
                return true
            }
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setContentView(R.layout.activity_settings)

        character = intent.extras!!.getSerializable("character") as Character

        repo = DamageRepository(application)

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener { view ->
            createNewDamage(character.id)
            Snackbar.make(view, "New Damage Added", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
            refreshData()
        }

        adapter = WeaponAdapter(application, R.layout.weapon_layout, ArrayList())
        val listingsView = findViewById<RecyclerView>(R.id.listings_view)
        listingsView.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this)
        listingsView.layoutManager = layoutManager
        listingsView.adapter = adapter

        refreshData()
    }

    private fun refreshData() {
        val fetchedDamage = repo.getDamagesForCharacter(character.id)
        fetchedDamage.observe(this, object : Observer<List<Damage>> {
            override fun onChanged(damages: List<Damage>?) {
                if (damages!!.size > 0) {
                    fetchedDamage.removeObserver(this)
                }
                adapter.settings = damages
                adapter.notifyDataSetChanged()
                if (damages.size == 0) {
                    val characterId = character.id
                    val damage = Damage()
                    damage.characterId = characterId
                    repo.insert(damage)
                }
            }
        })
    }

    private fun createNewDamage(id: Int) {
        val repository = DamageRepository(application)
        val damage = Damage()
        damage.characterId = id
        repository.insert(damage)
    }

    inner class WeaponAdapter(private val application: Application, private val itemResource: Int, internal var settings: List<Damage>) : RecyclerView.Adapter<WeaponHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeaponHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(this.itemResource, parent, false)
            return WeaponHolder(this.application, view)
        }

        override fun onBindViewHolder(holder: WeaponHolder, position: Int) {
            holder.bindBakery(settings[position])
        }

        override fun getItemCount(): Int {
            return this.settings.size
        }
    }
}
