package au.com.glen.shooter5000.database.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData

import au.com.glen.shooter5000.database.repository.CharacterRepository
import au.com.glen.shooter5000.database.model.Character

class CharacterViewModel(application: Application) : AndroidViewModel(application) {

    private val characterRepository: CharacterRepository

    val allCharacters: LiveData<List<Character>>

    init {
        characterRepository = CharacterRepository(application)
        allCharacters = characterRepository.allCharacters
    }

    fun getCharacter(charId: Int): LiveData<Character> {
        return characterRepository.getCharacter(charId)
    }

    fun insert(character: Character) {
        characterRepository.insert(character)
    }


}
