package au.com.glen.shooter5000.main

import android.content.Context
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.ImageView
import au.com.glen.shooter5000.R
import au.com.glen.shooter5000.database.model.Character
import au.com.glen.shooter5000.database.repository.CharacterRepository
import au.com.glen.shooter5000.modifiers.Modifier

class ModifierArrayAdapter(context: Context, var keys: List<Modifier>, var character: Character?, private val repo: CharacterRepository) : ArrayAdapter<Modifier>(context, R.layout.rowlayout, keys) {

    var values: BooleanArray = BooleanArray(keys.size)

    init {
        generateValues()
    }

    private fun generateValues() {
        if (character != null) {
            for (i in keys.indices) {
                values[i] = character!!.activatedModifiers.contains(keys[i].name)
            }
        }
    }

    override fun notifyDataSetChanged() {
        generateValues()
        super.notifyDataSetChanged()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        val inflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.rowlayout, parent, false)
        }
        val box = convertView!!.findViewById<CheckBox>(R.id.checkBox)
        box.isChecked = values[position]
        box.text = keys[position].name
        if (character != null) {
            box.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    character!!.activatedModifiers.add(keys[position].name)
                } else {
                    character!!.activatedModifiers.removeAll(listOf(keys[position].name))
                }
                values[position] = isChecked
                repo.update(character!!)
            }
        }

        val detailView = convertView.findViewById<ImageView>(R.id.detail_info)
        detailView.setOnClickListener {
            val builder1 = AlertDialog.Builder(context)
            builder1.setMessage(keys[position].description)
            builder1.setCancelable(true)

            val alert11 = builder1.create()
            alert11.show()
        }
        return convertView
    }

}