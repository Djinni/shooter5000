package au.com.glen.shooter5000.modifiers

import au.com.glen.shooter5000.database.model.Attack

class Bane : Modifier() {
    init {
        name = "Bane"
        description = "Your bow is especially adept at hunting the enemy creature type."
    }

    override fun modifyPreAttacks(attacks: MutableList<Attack>): List<Attack> {
        for (attack in attacks) {
            attack.modifier = attack.modifier + 2
            val primaryDamage = attack.damage[0]
            primaryDamage.damageModifier = primaryDamage.damageModifier + 2
            var dice: ArrayList<Int> = ArrayList()
            dice.add(6)
            dice.add(6)
            dice.addAll(primaryDamage.damageDice)
            primaryDamage.damageDice = dice
        }
        return attacks
    }
}
