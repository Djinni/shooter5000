package au.com.glen.shooter5000.database.model

import android.arch.persistence.room.*
import android.arch.persistence.room.ForeignKey.CASCADE
import au.com.glen.shooter5000.database.converters.IntListConverter
import java.io.Serializable
import java.util.*
import java.util.concurrent.ThreadLocalRandom

@Entity(tableName = "damage_table", foreignKeys = arrayOf(ForeignKey(entity = Character::class, parentColumns = arrayOf("id"), childColumns = arrayOf("characterId"), onDelete = CASCADE)))
class Damage : Serializable {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    var characterId: Int = 0

    var damageType: String = ""

    @TypeConverters(IntListConverter::class)
    var damageDice: ArrayList<Int> = ArrayList()

    var damageModifier: Int = 0

    var damageValue: Int = 0

    var multiplier: Int = 0

    fun rollDamage(critical: Boolean) {
        damageValue = 0
        for (damage in this.damageDice) {
            damageValue += ThreadLocalRandom.current().nextInt(1, damage + 1)
            if (critical) {
                for (i in 1 until multiplier) {
                    damageValue += ThreadLocalRandom.current().nextInt(1, damage + 1)
                }
            }
        }
        if (critical) {
            for (i in 1 until multiplier) {
                damageValue += damageModifier
            }
        }
        damageValue += damageModifier
    }

    fun copy(): Damage {
        val newDamage = Damage()
        newDamage.id = id
        newDamage.characterId = characterId
        newDamage.damageType = damageType
        newDamage.damageModifier = damageModifier
        newDamage.multiplier = multiplier
        newDamage.damageValue = damageValue
        if (damageDice != null) {
            val newDice = ArrayList<Int>()
            newDice.addAll(damageDice)
            newDamage.damageDice = newDice
        }
        return newDamage
    }

    operator fun setValue(key: String, value: Any, clazz: Class<*>) {
        try {
            val method = this.javaClass.getMethod(key, clazz)
            method.invoke(this, value)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false

        val damage = o as Damage?

        if (id != damage!!.id) return false
        if (characterId != damage.characterId) return false
        if (damageModifier != damage.damageModifier) return false
        if (damageValue != damage.damageValue) return false
        if (multiplier != damage.multiplier) return false
        if (if (damageType != null) damageType != damage.damageType else damage.damageType != null)
            return false
        return if (damageDice != null) damageDice == damage.damageDice else damage.damageDice == null
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + characterId
        result = 31 * result + if (damageType != null) damageType.hashCode() else 0
        result = 31 * result + if (damageDice != null) damageDice.hashCode() else 0
        result = 31 * result + damageModifier
        result = 31 * result + damageValue
        result = 31 * result + multiplier
        return result
    }
}
