package au.com.glen.shooter5000.modifiers

import au.com.glen.shooter5000.database.model.Attack

class MultiShot : Modifier() {
    init {
        name = "Multi Shot"
        description = "You fire two arrows instead of one with your first attack."
    }

    override fun modifyPostAttacks(attacks: MutableList<Attack>): List<Attack> {
        val first = attacks[0]
        attacks.add(0, first.copy())
        return attacks
    }
}
