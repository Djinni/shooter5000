package au.com.glen.shooter5000.database.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update

import au.com.glen.shooter5000.database.model.Damage

@Dao
interface DamageDao {

    @get:Query("SELECT * from damage_table")
    val allDamage: LiveData<List<Damage>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(damage: Damage)

    @Delete
    fun delete(damage: Damage)

    @Update
    fun update(damage: Damage)

    @Query("DELETE FROM damage_table")
    fun deleteAll()

    @Query("SELECT * from damage_table WHERE id = :damageId")
    fun getDamage(damageId: Int): LiveData<Damage>

    @Query("SELECT * from damage_table WHERE characterId = :characterId")
    fun getDamageForCharacter(characterId: Int): LiveData<List<Damage>>
}
