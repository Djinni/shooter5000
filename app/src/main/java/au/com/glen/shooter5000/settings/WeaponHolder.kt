package au.com.glen.shooter5000.settings


import android.annotation.SuppressLint
import android.app.Application
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText

import java.util.ArrayList

import au.com.glen.shooter5000.R
import au.com.glen.shooter5000.database.model.Damage
import au.com.glen.shooter5000.database.repository.DamageRepository

class WeaponHolder(context: Application, itemView: View) : RecyclerView.ViewHolder(itemView) {


    private val diceNum: EditText
    private val diceSize: EditText
    private val damageType: EditText
    private val critMultiplier: EditText


    private var damage: Damage? = null

    private enum class FieldType {
        INTEGER, STRING, INTEGERLIST
    }

    init {

        // 2. Set up the UI widgets of the holder
        this.diceNum = itemView.findViewById(R.id.numberEditText)
        this.diceSize = itemView.findViewById(R.id.diceEditText)
        this.damageType = itemView.findViewById(R.id.damageTypeEditText)
        this.critMultiplier = itemView.findViewById(R.id.multiplierEditText)

        this.diceNum.addTextChangedListener(WeaponTextWatcher("setDiceNum", context, FieldType.INTEGERLIST))
        this.diceSize.addTextChangedListener(WeaponTextWatcher("setDiceSize", context, FieldType.INTEGERLIST))
        this.damageType.addTextChangedListener(WeaponTextWatcher("setDamageType", context, FieldType.STRING))
        this.critMultiplier.addTextChangedListener(WeaponTextWatcher("setMultiplier", context, FieldType.INTEGER))
    }

    @SuppressLint("SetTextI18n")
    fun bindBakery(settings: Damage) {
        this.damage = settings
        this.diceNum.setText(Integer.toString(damage!!.damageDice.size))
        if (damage!!.damageDice.size > 0) {
            this.diceSize.setText(Integer.toString(damage!!.damageDice[0]))
        }
        this.damageType.setText(damage!!.damageType)
        this.critMultiplier.setText(damage!!.multiplier.toString())
    }

    private inner class WeaponTextWatcher internal constructor(private val methodName: String, private val context: Application, private val fieldType: FieldType) : TextWatcher {

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            try {
                val oldDamage = damage!!.copy()
                when (fieldType) {
                    WeaponHolder.FieldType.STRING -> damage!!.setValue(methodName, s.toString(), String::class.java)
                    WeaponHolder.FieldType.INTEGERLIST -> {
                        val diceType = Integer.parseInt(diceSize.text.toString())
                        damage!!.damageDice = ArrayList()
                        for (i in 0 until Integer.parseInt(diceNum.text.toString())) {
                            damage!!.damageDice.add(diceType)
                        }
                    }
                    WeaponHolder.FieldType.INTEGER -> {
                        val multiplier = Integer.parseInt(s.toString())
                        damage!!.setValue(methodName, multiplier, Int::class.javaPrimitiveType!!)
                    }
                }
                if (oldDamage != damage) {
                    DamageRepository(context).update(damage!!)
                }
            } catch (e: NumberFormatException) {
                e.printStackTrace()
            }

        }

        override fun afterTextChanged(s: Editable) {

        }
    }

}
