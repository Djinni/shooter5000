package au.com.glen.shooter5000.modifiers

import au.com.glen.shooter5000.database.model.Attack

class PointBlank : Modifier() {
    init {
        name = "Point Blank Shot"
        description = "Your arrows are more accurate and harder hitting at close range."
    }

    override fun modifyPreAttacks(attacks: MutableList<Attack>): List<Attack> {
        for (attack in attacks) {
            attack.modifier = attack.modifier + 1
            val firstDamage = attack.damage[0]
            firstDamage.damageModifier = firstDamage.damageModifier + 1
        }
        return attacks
    }
}
