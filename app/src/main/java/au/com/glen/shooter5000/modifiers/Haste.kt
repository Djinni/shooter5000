package au.com.glen.shooter5000.modifiers

import au.com.glen.shooter5000.database.model.Attack

class Haste : Modifier() {
    init {
        name = "Haste"
        description = "You move with supernatural speed, landing extra blows."
    }

    override fun modifyPreAttacks(attacks: MutableList<Attack>): List<Attack> {
        if (attacks.size > 0) {
            for (attack in attacks) {
                attack.modifier = attack.modifier + 1
            }
            attacks.add(0, attacks[0].copy())
        }
        return attacks
    }
}
