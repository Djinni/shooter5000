package au.com.glen.shooter5000.modifiers

import au.com.glen.shooter5000.database.model.Attack

open class Modifier {

    var description: String = ""

    var name: String = ""

    open fun modifyPreAttacks(attacks: MutableList<Attack>): List<Attack> {
        return attacks
    }

    open fun modifyPostAttacks(attacks: MutableList<Attack>): List<Attack> {
        return attacks
    }
}
