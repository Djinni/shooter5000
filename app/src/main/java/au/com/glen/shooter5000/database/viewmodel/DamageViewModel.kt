package au.com.glen.shooter5000.database.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData

import au.com.glen.shooter5000.database.model.Damage
import au.com.glen.shooter5000.database.repository.DamageRepository

class DamageViewModel(application: Application) : AndroidViewModel(application) {

    private val damageRepository: DamageRepository = DamageRepository(application)

    val allDamages: LiveData<List<Damage>>

    init {
        allDamages = damageRepository.allDamages
    }

    fun getDamage(charId: Int): LiveData<Damage> {
        return damageRepository.getDamage(charId)
    }

    fun getDamageForCharacter(charId: Int): LiveData<List<Damage>> {
        return damageRepository.getDamagesForCharacter(charId)
    }

    fun insert(Damage: Damage) {
        damageRepository.insert(Damage)
    }


}
