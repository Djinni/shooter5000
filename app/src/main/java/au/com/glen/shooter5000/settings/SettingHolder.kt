package au.com.glen.shooter5000.settings


import android.app.Application
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.TextView

import au.com.glen.shooter5000.R
import au.com.glen.shooter5000.database.model.Character
import au.com.glen.shooter5000.database.repository.CharacterRepository

class SettingHolder(context: Application, itemView: View) : RecyclerView.ViewHolder(itemView) {


    private val settingName: TextView
    private val settingValue: EditText

    private var setting: Character? = null

    internal var setMethod: String? = null

    init {

        // 2. Set up the UI widgets of the holder
        this.settingValue = itemView.findViewById(R.id.value)
        this.settingName = itemView.findViewById(R.id.numberLabel)

        this.settingValue.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                try {
                    if (setMethod != null) {
                        if (setMethod!!.equals("setName", ignoreCase = true)) {
                            setting!!.setValue(setMethod!!, s.toString(), String::class.java)
                        } else {
                            setting!!.setValue(setMethod!!, Integer.parseInt(s.toString()), Int::class.java)
                        }
                        CharacterRepository(context).update(setting!!)
                    }
                } catch (e: NumberFormatException) {
                    e.printStackTrace()
                }

            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    fun bindBakery(settings: Character, position: Int) {
        this.setting = settings
        val settingsList = settings.generateSettings()


        this.settingName.text = settingsList[0][position]
        this.settingValue.setText(settingsList[1][position])
        this.setMethod = settingsList[2][position]
        if (this.setMethod!!.equals("setName", ignoreCase = true)) {
            this.settingValue.inputType = InputType.TYPE_CLASS_TEXT
        } else {
            this.settingValue.inputType = InputType.TYPE_CLASS_NUMBER
        }
    }

}
