package au.com.glen.shooter5000.modifiers

import au.com.glen.shooter5000.database.model.Attack

class DeadlyShot : Modifier() {
    init {
        description = "You sacrifice accuracy for increased damage."
        name = "Deadly Aim"
    }

    override fun modifyPreAttacks(attacks: MutableList<Attack>): List<Attack> {
        for (attack in attacks) {
            attack.modifier = attack.modifier - 2
            val primaryDamage = attack.damage[0]
            primaryDamage.damageModifier = primaryDamage.damageModifier + 4
        }
        return attacks
    }
}
