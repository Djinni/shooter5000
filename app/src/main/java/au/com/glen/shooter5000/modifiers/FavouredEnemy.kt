package au.com.glen.shooter5000.modifiers

import au.com.glen.shooter5000.database.model.Attack

class FavouredEnemy(private val favouredEnemyModifier: Int, name: String) : Modifier() {

    init {
        this.name = "Favoured Enemy ($name)"
        description = "You are skilled at hunting " + name + "s"
    }

    override fun modifyPreAttacks(attacks: MutableList<Attack>): List<Attack> {
        for (attack in attacks) {
            attack.modifier = attack.modifier + favouredEnemyModifier
            val firstDamage = attack.damage[0]
            firstDamage.damageModifier = firstDamage.damageModifier + favouredEnemyModifier
        }
        return attacks
    }


}
