package au.com.glen.shooter5000.modifiers

import au.com.glen.shooter5000.database.model.Attack

class Disadvantage : Modifier() {

    override fun modifyPreAttacks(attacks: MutableList<Attack>): List<Attack> {
        for (attack in attacks) {
            attack.advantage = -1
        }
        return attacks
    }
}
