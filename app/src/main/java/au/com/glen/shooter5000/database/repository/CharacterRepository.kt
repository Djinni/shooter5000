package au.com.glen.shooter5000.database.repository

import android.app.Application
import android.arch.lifecycle.LiveData
import android.os.AsyncTask

import au.com.glen.shooter5000.database.model.Character
import au.com.glen.shooter5000.database.dao.CharacterDao
import au.com.glen.shooter5000.database.CharacterRoomDatabase

class CharacterRepository(application: Application) {
    private val characterDao: CharacterDao

    val allCharacters: LiveData<List<Character>>

    init {
        val db = CharacterRoomDatabase.getDatabase(application)
        characterDao = db.characterDao()
        allCharacters = characterDao.allCharacters
    }

    fun getCharacter(charId: Int): LiveData<Character> {
        return characterDao.getCharacter(charId)
    }

    fun insert(character: Character) {
        InsertAsyncTask(characterDao).execute(character)
    }

    fun update(character: Character) {
        UpdateAsyncTask(characterDao).execute(character)
    }

    private class InsertAsyncTask internal constructor(private val mAsyncTaskDao: CharacterDao) : AsyncTask<Character, Void, Void>() {

        override fun doInBackground(vararg params: Character): Void? {
            mAsyncTaskDao.insert(params[0])
            return null
        }
    }

    private class UpdateAsyncTask internal constructor(private val mAsyncTaskDao: CharacterDao) : AsyncTask<Character, Void, Void>() {

        override fun doInBackground(vararg params: Character): Void? {
            mAsyncTaskDao.update(params[0])
            return null
        }
    }
}


