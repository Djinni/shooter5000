package au.com.glen.shooter5000.settings

import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.ViewGroup
import au.com.glen.shooter5000.R
import au.com.glen.shooter5000.database.model.Character

class AttackSettings : AppCompatActivity() {

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            0 -> {
                val launchProductList = Intent(this, AttackSettings::class.java)
                startActivity(launchProductList)
                return true
            }
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
        setContentView(R.layout.activity_settings)

        val character = intent.extras!!.getSerializable("character") as Character

        val adapter = SettingAdapter(this.application, R.layout.setting_layout, character)
        val listingsView = findViewById<RecyclerView>(R.id.listings_view)
        listingsView.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(this)
        listingsView.layoutManager = layoutManager
        listingsView.adapter = adapter
    }

    private inner class SettingAdapter(private val application: Application, private val itemResource: Int, private val settings: Character) : RecyclerView.Adapter<SettingHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(this.itemResource, parent, false)
            return SettingHolder(this.application, view)
        }

        override fun onBindViewHolder(holder: SettingHolder, position: Int) {
            holder.bindBakery(settings, position)
        }

        override fun getItemCount(): Int {
            return this.settings.generateSettings()[0].size
        }
    }
}
