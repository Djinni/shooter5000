package au.com.glen.shooter5000.database.model

import java.util.ArrayList
import java.util.concurrent.ThreadLocalRandom

import au.com.glen.shooter5000.modifiers.Modifier

class Attack {

    var modifier = 0

    var advantage = 0

    var damage: MutableList<Damage> = mutableListOf()

    var attackValue = 0

    var isCritical = false

    fun makeAttack() {
        var roll1 = ThreadLocalRandom.current().nextInt(1, 21)
        val roll2 = ThreadLocalRandom.current().nextInt(1, 21)
        val confirm = ThreadLocalRandom.current().nextInt(1, 21)

        if (advantage == 1) {
            if (roll2 > roll1) {
                roll1 = roll2
            }
        } else if (advantage == -1) {
            if (roll2 < roll1) {
                roll1 = roll2
            }
        }
        this.attackValue = roll1 + modifier

        for (damage in damage) {
            damage.rollDamage(roll1 == 20)
        }
        if (roll1 == 20) {
            this.isCritical = true
            this.attackValue = confirm + modifier
        }
    }

    fun copy(): Attack {
        val newAttack = Attack()
        newAttack.modifier = modifier
        newAttack.advantage = advantage
        newAttack.attackValue = attackValue
        newAttack.isCritical = isCritical
        val newDamage = ArrayList<Damage>()
        for (damageValue in damage) {
            newDamage.add(damageValue.copy())
        }
        newAttack.damage = newDamage
        return newAttack
    }

    companion object {

        fun implementAttacks(modifiers: MutableList<Modifier>, attacks: MutableList<Attack>): MutableList<Attack> {
            for (i in modifiers.indices) {
                modifiers[i].modifyPreAttacks(attacks)
            }

            for (attack in attacks) {
                attack.makeAttack()
            }

            for (i in modifiers.indices) {
                modifiers[i].modifyPostAttacks(attacks)
            }
            return attacks
        }

        fun makeAttacks(modifiers: MutableList<Modifier>, attacks: MutableList<Attack>): String {
            implementAttacks(modifiers, attacks)
            return attackStringBuilder(attacks)
        }

        private fun attackStringBuilder(attacks: List<Attack>): String {
            val attackResult = StringBuilder()
            for (attack in attacks) {
                if (attack.isCritical) {
                    attackResult.append("CRITICAL! ")
                }
                attackResult.append(attack.attackValue).append(" for ")
                var first = true
                for (damage in attack.damage) {
                    if (!first) {
                        attackResult.append(" and ")
                    }
                    first = false
                    attackResult.append(damage.damageValue).append(" ").append(damage.damageType)
                }
                attackResult.append("\n")
            }
            return attackResult.toString()
        }
    }
}
