package au.com.glen.shooter5000.modifiers

import au.com.glen.shooter5000.database.model.Attack

class Prayer : Modifier() {
    init {
        name = "Prayer"
        description = "You receive +1 to hit and damage."
    }

    override fun modifyPreAttacks(attacks: MutableList<Attack>): List<Attack> {
        for (attack in attacks) {
            attack.modifier = attack.modifier + 1
            val firstDamage = attack.damage[0]
            firstDamage.damageModifier = firstDamage.damageModifier + 1
        }
        return attacks
    }
}
