package au.com.glen.shooter5000.modifiers

import au.com.glen.shooter5000.database.model.Attack

class BullsStrength : Modifier() {
    init {
        name = "Bulls Strength"
        description = "Your strength is magically enhanced."
    }

    override fun modifyPreAttacks(attacks: MutableList<Attack>): List<Attack> {
        for (attack in attacks) {
            val firstDamage = attack.damage[0]
            firstDamage.damageModifier = firstDamage.damageModifier + 2
        }
        return attacks
    }
}
