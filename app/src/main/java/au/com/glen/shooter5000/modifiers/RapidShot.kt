package au.com.glen.shooter5000.modifiers

import au.com.glen.shooter5000.database.model.Attack

class RapidShot : Modifier() {
    init {
        name = "Rapid Shot"
        description = "You focus your energy on unleashing a hail of arrows."
    }

    override fun modifyPreAttacks(attacks: MutableList<Attack>): List<Attack> {
        if (attacks.size > 0) {
            for (attack in attacks) {
                attack.modifier = attack.modifier - 2
            }
            attacks.add(0, attacks[0].copy())
        }
        return attacks
    }
}
