package au.com.glen.shooter5000.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ListView
import android.widget.TextView
import au.com.glen.shooter5000.R
import au.com.glen.shooter5000.database.model.Attack
import au.com.glen.shooter5000.database.model.Character
import au.com.glen.shooter5000.database.model.Damage
import au.com.glen.shooter5000.database.repository.CharacterRepository
import au.com.glen.shooter5000.database.viewmodel.CharacterViewModel
import au.com.glen.shooter5000.database.viewmodel.DamageViewModel
import au.com.glen.shooter5000.modifiers.*
import au.com.glen.shooter5000.settings.AttackSettings
import au.com.glen.shooter5000.settings.WeaponSettings
import java.util.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var selectedCharacter: Character? = null

    private var characterDamage: List<Damage>? = null

    private var characterViewModel: CharacterViewModel? = null

    private var damageViewModel: DamageViewModel? = null

    private var adapter: ModifierArrayAdapter? = null

    override fun onResume() {
        super.onResume()

        val p = PreferenceManager.getDefaultSharedPreferences(this)
        val lastCharacter = p.getInt("lastChar", 0)
        fetchCharacter(lastCharacter)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        characterViewModel = ViewModelProviders.of(this).get(CharacterViewModel::class.java)
        damageViewModel = ViewModelProviders.of(this).get(DamageViewModel::class.java)
        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener { view ->
            createNewCharacter()
            Snackbar.make(view, "New Character Added", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        val modifierArray = arrayOf(Bless(), DeadlyShot(), MultiShot(), PointBlank(), Prayer(), RapidShot(), FavouredEnemy(2, "Aberration"), FavouredEnemy(4, "Magical Beast"), Bane(), Haste(), BullsStrength())

        val modifiers = Arrays.asList(*modifierArray)

        val table = this.findViewById<ListView>(R.id.option_table)
        adapter = ModifierArrayAdapter(this, modifiers, selectedCharacter, CharacterRepository(application))
        table.adapter = adapter

        findViewById<View>(R.id.attackButton).setOnClickListener {
            val mp = MediaPlayer.create(applicationContext, R.raw.bow)
            mp.start()
            if (selectedCharacter != null) {
                makeAttacks(modifiers, adapter!!.values)
            }
        }

        findViewById<View>(R.id.AttackInfo).setOnClickListener {
            if (selectedCharacter != null) {
                val attacks = fetchAttacks()
                val activatedModifiers = ArrayList<Modifier>()
                for (i in modifiers.indices) {
                    if (adapter!!.values[i]) {
                        activatedModifiers.add(modifiers[i])
                    }
                }

                val modifiedAttacks = Attack.implementAttacks(activatedModifiers, attacks)
                val message = StringBuilder()
                for (attack in modifiedAttacks) {
                    message.append("To hit: +").append(attack.modifier).append(" Damage: +").append(attack.damage[0].damageModifier).append("\n")
                }

                val builder1 = AlertDialog.Builder(this@MainActivity)
                builder1.setMessage(message.toString())
                builder1.setCancelable(true)

                val alert11 = builder1.create()
                alert11.show()
            }
        }

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        setupMenu()
    }

    private fun makeAttacks(modifiers: List<Modifier>, values: BooleanArray) {
        val attacks = fetchAttacks()
        val activatedModifiers = ArrayList<Modifier>()
        for (i in modifiers.indices) {
            if (values[i]) {
                activatedModifiers.add(modifiers[i])
            }
        }

        val attackString = Attack.makeAttacks(activatedModifiers, attacks)

        val result = this@MainActivity.findViewById<TextView>(R.id.attackResult)
        result.text = attackString
    }

    private fun createNewCharacter() {
        val repository = CharacterRepository(application)
        val character = Character()
        character.name = "New Character"
        repository.insert(character)
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        val menu = navigationView.menu
        menu.add(R.id.character_group, character.id, 0, character.name)
    }

    private fun setupMenu() {
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)
        val menu = navigationView.menu

        characterViewModel!!.allCharacters.observe(this, Observer { characters ->
            // Update the characters in the menu.
            menu.clear()
            for (character in characters!!) {
                val item = menu.add(R.id.character_group, character.id, 0, character.name)
                item.isCheckable = true
                if (selectedCharacter != null) {
                    if (character.id == selectedCharacter!!.id) {
                        item.isChecked = true
                    }
                }
            }
        })
    }

    private fun fetchAttacks(): MutableList<Attack> {
        val attacks : MutableList<Attack> = mutableListOf()

        val attackChar = selectedCharacter
        val baseAttack = if (attackChar!!.baseAttack == null) 0 else attackChar.baseAttack

        var attackModifier = if (attackChar.dexterity == null) 0 else attackChar.dexterity
        attackModifier += if (attackChar.feats == null) 0 else attackChar.feats
        attackModifier += if (attackChar.enchantmentBonus == null) 0 else attackChar.enchantmentBonus

        var damageModifier = if (attackChar.strength == null) 0 else attackChar.strength
        damageModifier += if (attackChar.enchantmentBonus == null) 0 else attackChar.enchantmentBonus
        var i = baseAttack
        while (i > 0) {
            attacks.add(fetchAttack(attackModifier + i, damageModifier))
            i -= 5
        }

        return attacks
    }

    private fun fetchAttack(attackModifier: Int, damageModifier: Int): Attack {
        val bowAttack = Attack()
        bowAttack.damage = mutableListOf()
        for (damage in characterDamage!!) {
            bowAttack.damage.add(damage.copy())
        }
        if (bowAttack.damage.size > 0) {
            bowAttack.damage[0].damageModifier = damageModifier
        }
        bowAttack.modifier = attackModifier
        return bowAttack
    }

    fun fetchCharacter(id: Int) {
        fetchDamage(id)
        val fetchedCharacter = characterViewModel!!.getCharacter(id)
        fetchedCharacter.observe(this, object : Observer<Character> {
            override fun onChanged(character: Character?) {
                if (character != null) {
                    selectedCharacter = character
                    fetchedCharacter.removeObserver(this)
                    this@MainActivity.title = character.name
                    (this@MainActivity.findViewById<View>(R.id.nav_view) as NavigationView).setCheckedItem(character.id)
                    adapter!!.character = character
                    adapter!!.notifyDataSetChanged()
                }
            }
        })
    }

    fun fetchDamage(id: Int) {
        val fetchedDamage = damageViewModel!!.getDamageForCharacter(id)
        fetchedDamage.observe(this, object : Observer<List<Damage>> {
            override fun onChanged(damage: List<Damage>?) {
                if (damage != null) {
                    characterDamage = damage
                    fetchedDamage.removeObserver(this)
                }
            }
        })
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        val p = PreferenceManager.getDefaultSharedPreferences(this)
        p.edit().putInt("lastChar", id).apply()
        fetchCharacter(id)

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.action_settings -> {
                if (selectedCharacter != null) {
                    val launchProductList = Intent(this, AttackSettings::class.java)
                    val character = selectedCharacter
                    launchProductList.putExtra("character", character)
                    startActivity(launchProductList)
                    return super.onOptionsItemSelected(item)
                }
                return true
            }
            R.id.action_weapon -> {
                if (selectedCharacter != null) {
                    val launchProductList = Intent(this, WeaponSettings::class.java)
                    val character = selectedCharacter
                    launchProductList.putExtra("character", character)
                    startActivity(launchProductList)
                    return super.onOptionsItemSelected(item)
                }
                return true
            }
        }
        return true
    }
}
