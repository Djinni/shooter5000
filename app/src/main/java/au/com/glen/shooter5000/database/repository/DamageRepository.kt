package au.com.glen.shooter5000.database.repository

import android.app.Application
import android.arch.lifecycle.LiveData
import android.os.AsyncTask

import au.com.glen.shooter5000.database.dao.DamageDao
import au.com.glen.shooter5000.database.CharacterRoomDatabase
import au.com.glen.shooter5000.database.model.Damage

class DamageRepository(application: Application) {
    private val damageDao: DamageDao

    val allDamages: LiveData<List<Damage>>

    init {
        val db = CharacterRoomDatabase.getDatabase(application)
        damageDao = db.damageDao()
        allDamages = damageDao.allDamage
    }

    fun getDamage(damageId: Int): LiveData<Damage> {
        return damageDao.getDamage(damageId)
    }

    fun getDamagesForCharacter(characterId: Int): LiveData<List<Damage>> {
        return damageDao.getDamageForCharacter(characterId)
    }

    fun insert(damage: Damage) {
        InsertAsyncTask(damageDao).execute(damage)
    }

    fun update(damage: Damage) {
        UpdateAsyncTask(damageDao).execute(damage)
    }

    private class InsertAsyncTask internal constructor(private val mAsyncTaskDao: DamageDao) : AsyncTask<Damage, Void, Void>() {

        override fun doInBackground(vararg params: Damage): Void? {
            mAsyncTaskDao.insert(params[0])
            return null
        }
    }

    private class UpdateAsyncTask internal constructor(private val mAsyncTaskDao: DamageDao) : AsyncTask<Damage, Void, Void>() {

        override fun doInBackground(vararg params: Damage): Void? {
            mAsyncTaskDao.update(params[0])
            return null
        }
    }
}


