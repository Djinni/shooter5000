package au.com.glen.shooter5000.database.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import au.com.glen.shooter5000.database.converters.StringListConverter
import java.io.Serializable
import java.util.*

@Entity(tableName = "character_table")
class Character : Serializable {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    var name: String = ""

    var strength: Int = 0

    var dexterity: Int = 0

    var baseAttack: Int = 0

    var feats: Int = 0

    var enchantmentBonus: Int = 0

    @TypeConverters(StringListConverter::class)
    var activatedModifiers: ArrayList<String> = ArrayList()

    fun generateSettings(): List<List<String>> {
        val keyList = Arrays.asList("Name", "Strength", "Dexterity", "Base Attack Bonus", "Feats", "EnchantmentBonus")
        val valueList = Arrays.asList<String>(name, if (strength != null) strength.toString() else null, if (dexterity != null) dexterity.toString() else null, if (baseAttack != null) baseAttack.toString() else null, if (feats != null) feats.toString() else null, if (enchantmentBonus != null) enchantmentBonus.toString() else null)
        val setterList = Arrays.asList("setName", "setStrength", "setDexterity", "setBaseAttack", "setFeats", "setEnchantmentBonus")
        return Arrays.asList(keyList, valueList, setterList)
    }

    operator fun setValue(key: String, value: Any, clazz: Class<*>) {
        try {
            val method = this.javaClass.getMethod(key, clazz)
            method.invoke(this, value)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}
