package au.com.glen.shooter5000.database.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update

import au.com.glen.shooter5000.database.model.Character

@Dao
interface CharacterDao {

    @get:Query("SELECT * from character_table ORDER BY name ASC")
    val allCharacters: LiveData<List<Character>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(character: Character)

    @Delete
    fun delete(character: Character)

    @Update
    fun update(character: Character)

    @Query("DELETE FROM character_table")
    fun deleteAll()

    @Query("SELECT * from character_table WHERE id = :charId ORDER BY name ASC")
    fun getCharacter(charId: Int): LiveData<Character>
}
